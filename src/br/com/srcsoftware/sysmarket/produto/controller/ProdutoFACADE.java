package br.com.srcsoftware.sysmarket.produto.controller;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.exceptions.RollbackException;
import br.com.srcsoftware.sysmarket.produto.model.ProdutoPO;
import br.com.srcsoftware.sysmarket.produto.model.ProdutoSERVICE;

public class ProdutoFACADE {
	/**
	 * Atributo responsável por possibilitar o acesso da Camada Controller a
	 * Camada MODEL
	 */
	private final ProdutoSERVICE SERVICE;

	// ********************************************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************************************** \\
	private static ProdutoFACADE instance;

	private ProdutoFACADE() {
		SERVICE = new ProdutoSERVICE();
	}

	public static ProdutoFACADE getInstance() {
		if (instance == null) {
			instance = new ProdutoFACADE();
		}
		return instance;
	}

	// ********************* FIM ******************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************* FIM ******************** \\

	public void inserir(ProdutoPO po) throws ApplicationException,
			InserirException, RollbackException {
		SERVICE.inserir(po);
	}

	public void alterar(ProdutoPO po) throws ApplicationException, RollbackException {
		SERVICE.alterar(po);
	}

	public void excluir(ProdutoPO po) throws ApplicationException, RollbackException {
		SERVICE.excluir(po);
	}

	public ArrayList<ProdutoPO> filtrar(ProdutoPO po)
			throws ApplicationException {
		return SERVICE.filtrar(po);
	}
}

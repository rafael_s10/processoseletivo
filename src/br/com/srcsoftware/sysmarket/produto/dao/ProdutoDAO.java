package br.com.srcsoftware.sysmarket.produto.dao;

import java.util.ArrayList;

import org.hibernate.Query;

import br.com.srcsoftware.sysmarket.abstracts.AbstractDAO;
import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.produto.model.ProdutoPO;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

public class ProdutoDAO extends AbstractDAO {
	@Override
	public ArrayList<AbstractPO> filtrar(AbstractPO abstractPO)
			throws FiltrarException {

		if (Utilidades.isNuloOuVazio(abstractPO)) {
			abstractPO = new ProdutoPO();
		}

		ProdutoPO po = null;

		/* Verificando se o abstractPO � do tipo ContatoPO */
		if (abstractPO instanceof ProdutoPO) {
			po = (ProdutoPO) abstractPO;
		} else {
			throw new FiltrarException(
					"Objeto informado n�o condiz com o contexto: "
							+ abstractPO.getClass());
		}

		try {
			// Criando uma HQL(Hibernate Query Language)
			final StringBuffer HQL = new StringBuffer();
			HQL.append("SELECT produto FROM ProdutoPO produto");
			HQL.append(" ");
			HQL.append("WHERE 1=1");

			montarHQLPrincipal(HQL, po);

			/*
			 * Criando e inicializando uma variavel respons�vel por criar uma
			 * Query com base na nossa HQL criada acima deixando-a preparada
			 * para o Hibernate executa-la.
			 */
			Query query = sessaoCorrente.createQuery(HQL.toString());

			/* Preenchendo os coringas da HQL */
			preecherCoringasHQLPrincipal(query, po);

			/*
			 * Executando a consulta list() = Consulta mais que um
			 * uniqueResult() = Consulta apenas 1 registro. Caso a HQL resulte
			 * em mais de um, uma excess�o ser� lan�ada.
			 */
			return (ArrayList<AbstractPO>) query.list();
		} catch (Exception e) {
			throw new FiltrarException("Erro ao filtrar", e);
		}

	}

	private void montarHQLPrincipal(StringBuffer HQL, ProdutoPO produto) {

		if (!Utilidades.isNuloOuVazio(produto.getCodigo())) {
			HQL.append(" ");
			HQL.append(" AND produto.codigo = :codigoParam");
		}

		if (!Utilidades.isNuloOuVazio(produto.getNome())) {
			HQL.append(" ");
			HQL.append(" AND produto.nome = :nomeParam");
		}

		if (!Utilidades.isNuloOuVazio(produto.getPreco())) {
			HQL.append(" ");
			HQL.append(" AND produto.preco = :precoParam");
		}

		if (!Utilidades.isNuloOuVazio(produto.getQuantidade())) {
			HQL.append(" ");
			HQL.append(" AND produto.quantidade = :quantidadeParam");
		}

		/** Verificando se a unidade foi enviado */
		if (!Utilidades.isNuloOuVazio(produto.getUnidadeDeMedida())) {
			montarHQLUnidade(HQL, produto.getUnidadeDeMedida());
		}

		HQL.append(" ");
		HQL.append("order by produto.nome");
	}

	private void montarHQLUnidade(StringBuffer HQL, UnidadeDeMedidaPO unidade) {
		if (!Utilidades.isNuloOuVazio(unidade.getNome())) {
			HQL.append(" ");
			HQL.append(" AND produto.unidade.nome = :nomeParam");
		}

		if (!Utilidades.isNuloOuVazio(unidade.getSigla())) {
			HQL.append(" ");
			HQL.append(" AND produto.unidade.sigla = :siglaParam");
		}

	}

	private void preecherCoringasHQLPrincipal(Query query, ProdutoPO produto) {

		if (!Utilidades.isNuloOuVazio(produto.getCodigo())) {
			query.setLong("codigoParam", produto.getCodigo());
		}

		if (!Utilidades.isNuloOuVazio(produto.getNome())) {
			query.setString("nomeParam", produto.getNome());
		}

		if (!Utilidades.isNuloOuVazio(produto.getPreco())) {
			query.setBigDecimal("precoParam", produto.getPreco());
		}

		if (!Utilidades.isNuloOuVazio(produto.getQuantidade())) {
			query.setBigDecimal("quantidadeParam", produto.getQuantidade());
		}

		/** Verificando se a Turma foi enviado */
		if (!Utilidades.isNuloOuVazio(produto.getUnidadeDeMedida())) {
			preecherCoringasHQLUnidadeDeMedida(query,
					produto.getUnidadeDeMedida());
		}
	}

	private void preecherCoringasHQLUnidadeDeMedida(Query query,
			UnidadeDeMedidaPO unidade) {
		if (!Utilidades.isNuloOuVazio(unidade.getNome())) {
			query.setString("nomeParam", unidade.getNome());
		}

		if (!Utilidades.isNuloOuVazio(unidade.getSigla())) {
			query.setString("siglaParam", unidade.getSigla());
		}

	}
}

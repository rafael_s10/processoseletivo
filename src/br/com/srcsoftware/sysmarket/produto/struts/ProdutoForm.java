package br.com.srcsoftware.sysmarket.produto.struts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.util.LabelValueBean;

import br.com.srcsoftware.sysmarket.produto.model.ProdutoPO;
import br.com.srcsoftware.sysmarket.unidadedemedida.controller.UnidadeDeMedidaFacade;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

/**
 * Classe respons�vel por conter um atributo para cada campo na tela(JSP). Todas
 * as altera��es que quiser fazer nos campos da tela, dever�o ser feitas no
 * FORM. Isso porque a tela � carregada com base numa classe FORM, sendo assim,
 * tudo que fizer na classe FORM ser� refletido nos campos da tela.
 * 
 * 
 * 
 * 
 * @author Rafael Guimaraes Santos <rafael_s10@live.com>
 * @since 23/01/2015 09:08:47
 * @version 1.0
 */

public final class ProdutoForm extends ActionForm {

	/** Declarando os atributos que representarao os campos da tela (jsp) */
	private String codigo;
	private String preco;
	private String nome;
	private String quantidade;

	// atributos relacionados a cHAVE ESTRANGEIRA
	private String sigla;
	private String nomeUnidade;
	private String codigoUnidade;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(String codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	ArrayList<ProdutoPO> listaProduto = new ArrayList<ProdutoPO>();

	public String getPreco() {
		return preco;
	}

	public void setPreco(String preco) {
		this.preco = preco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNomeUnidade() {
		return nomeUnidade;
	}

	public void setNomeUnidade(String nomeUnidade) {
		this.nomeUnidade = nomeUnidade;
	}

	public ArrayList<ProdutoPO> getListaProduto() {
		return listaProduto;
	}

	public void setListaProduto(ArrayList<ProdutoPO> listaProduto) {
		this.listaProduto = listaProduto;
	}

	public ArrayList<LabelValueBean> getComboProduto() throws Throwable {
		ArrayList<LabelValueBean> lista = new ArrayList<LabelValueBean>();

		try {
			ArrayList<UnidadeDeMedidaPO> objetosEncontrados = UnidadeDeMedidaFacade.getInstance().filtrar(null);
			Iterator<UnidadeDeMedidaPO> setIter = objetosEncontrados.iterator();
			LabelValueBean lvb = new LabelValueBean();
			lvb.setLabel("Selecione...");
			lvb.setValue(null);
			lista.add(lvb);

			while (setIter.hasNext()) {
				UnidadeDeMedidaPO unidade = (UnidadeDeMedidaPO) setIter.next();
				lvb = new LabelValueBean();

				lvb.setLabel(unidade.getNome() + "-" + unidade.getCodigo());
				lvb.setValue(unidade.getCodigo().toString());

				lista.add(lvb);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public void preencherProduto(ProdutoPO poEncontrado) throws ParseException, java.text.ParseException {
		this.setCodigo(poEncontrado.getCodigo().toString());
		this.setCodigoUnidade(poEncontrado.getUnidadeDeMedida().getCodigo().toString());
		this.setNome(poEncontrado.getNome());
		this.setNomeUnidade(poEncontrado.getUnidadeDeMedida().getNome());
		this.setPreco(poEncontrado.getPreco().toString());
		this.setQuantidade(poEncontrado.getQuantidade().toString());
		this.setSigla(poEncontrado.getUnidadeDeMedida().getSigla());

	}

	public ProdutoPO montarPO() throws ParseException, java.text.ParseException {
		ProdutoPO po = new ProdutoPO();

		if (!Utilidades.isNuloOuVazio(this.getCodigo())) {
			po.setCodigo(Long.valueOf(this.getCodigo()));

		}

		if (!Utilidades.isNuloOuVazio(this.getQuantidade())) {
			po.setQuantidade(new BigDecimal(this.getQuantidade()));

		}

		if (!Utilidades.isNuloOuVazio(this.getPreco())) {
			po.setPreco(new BigDecimal(this.getPreco()));

		}
		po.setNome(this.getNome());

		/** montando Unidade */
		po.setUnidadeDeMedida(new UnidadeDeMedidaPO());

		if (!Utilidades.isNuloOuVazio(getCodigoUnidade())) {
			po.getUnidadeDeMedida().setCodigo(Long.valueOf(getCodigoUnidade()));
		}

		po.getUnidadeDeMedida().setNome(this.getNome());
		po.getUnidadeDeMedida().setSigla(this.getSigla());
		return po;
	}

	public void limparCampos() {
		setCodigo("");
		setNome("");
		setQuantidade("");
		setCodigoUnidade("");
		setNomeUnidade("");
		setPreco("");
		setSigla("");

	}

}

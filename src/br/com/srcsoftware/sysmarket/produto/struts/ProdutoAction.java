package br.com.srcsoftware.sysmarket.produto.struts;

import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.produto.controller.ProdutoFACADE;
import br.com.srcsoftware.sysmarket.produto.model.ProdutoPO;
import br.com.srcsoftware.sysmarket.utilidades.Messages;

public final class ProdutoAction extends DispatchAction {
	public ActionForward abrirCadastro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/** fazendo o casting do form */
		ProdutoForm meuForm = (ProdutoForm) form;
		meuForm.limparCampos();

		return mapping.findForward("produto_formulario");
	}

	public ActionForward inserir(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		ProdutoForm meuForm = (ProdutoForm) form;
		try {
			ProdutoFACADE.getInstance().inserir(meuForm.montarPO());
			meuForm.limparCampos();
			this.addMessages(request, Messages.createMessages("sucesso"));

		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {
			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));

		}

		return mapping.findForward("produto_formulario");
	}

	public ActionForward alterar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		ProdutoForm meuForm = (ProdutoForm) form;
		try {
			ProdutoFACADE.getInstance().alterar(meuForm.montarPO());

			this.addMessages(request, Messages.createMessages("sucesso"));
		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {

			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));
		}

		return mapping.findForward("produto_formulario");
	}

	public ActionForward excluir(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		ProdutoForm meuForm = (ProdutoForm) form;
		try {
			ProdutoFACADE.getInstance().excluir(meuForm.montarPO());
			meuForm.limparCampos();

			this.addMessages(request, Messages.createMessages("sucesso"));
		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {

			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));
		}

		return mapping.findForward("produto_formulario");
	}

	public ActionForward novo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/** fazendo o casting do form */
		ProdutoForm meuForm = (ProdutoForm) form;

		meuForm.limparCampos();

		return mapping.findForward("produto_formulario");
	}

	public ActionForward selecionar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		ProdutoForm meuForm = (ProdutoForm) form;
		try {
			ArrayList<ProdutoPO> encontrados = ProdutoFACADE.getInstance()
					.filtrar(meuForm.montarPO());
			ProdutoPO selecionado = encontrados.get(0);
			meuForm.preencherProduto(selecionado);

		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {

			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));
		}

		return mapping.findForward("produto_formulario");
	}

	public ActionForward filtrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		ProdutoForm meuForm = (ProdutoForm) form;

		try {
			ArrayList<ProdutoPO> encontrados = ProdutoFACADE.getInstance()
					.filtrar(meuForm.montarPO());

			meuForm.setListaProduto(encontrados);

		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {

			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));
		}

		return mapping.findForward("produto_tabela");
	}
}

package br.com.srcsoftware.sysmarket.produto.model;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.AlterarException;
import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.exceptions.CommitException;
import br.com.srcsoftware.sysmarket.exceptions.ExcluirException;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.exceptions.RollbackException;
import br.com.srcsoftware.sysmarket.exceptions.SessaoException;
import br.com.srcsoftware.sysmarket.exceptions.TransacaoException;
import br.com.srcsoftware.sysmarket.interfaces.InterfaceDAO;
import br.com.srcsoftware.sysmarket.produto.dao.ProdutoDAO;

public class ProdutoSERVICE {
	private final InterfaceDAO DAO;

	public ProdutoSERVICE() {
		DAO = new ProdutoDAO();
	}

	public void inserir(ProdutoPO po) throws ApplicationException,
			InserirException, RollbackException {
		try {

			DAO.iniciarTransacao();

			DAO.inserir(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (SessaoException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (CommitException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (Exception e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(
					"Erro desconhecido, chame o suporte! (014 9.9678-9454)", e);
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
				throw new ApplicationException(e.getMessage(), e);
			}
		}

	}

	/**
	 * Instanciando o DAO
	 * 
	 * @throws ApplicationException
	 */
	public void alterar(ProdutoPO po) throws ApplicationException, RollbackException {
		try {

			DAO.iniciarTransacao();

			DAO.alterar(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (SessaoException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (CommitException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (AlterarException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (Exception e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(
					"Erro desconhecido, chame o suporte! (014 9.9678-9454)", e);
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
				throw new ApplicationException(e.getMessage(), e);
			}
		}

	}

	/**
	 * Instanciando o DAO
	 * 
	 * @throws ApplicationException
	 */
	public void excluir(ProdutoPO po) throws ApplicationException, RollbackException {
		try {
			DAO.iniciarTransacao();

			DAO.excluir(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (SessaoException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (CommitException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (ExcluirException e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (Exception e) {
			DAO.rollbackTransacao();
			e.printStackTrace();
			throw new ApplicationException(
					"Erro desconhecido, chame o suporte! (014 9.9678-9454)", e);
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
				throw new ApplicationException(e.getMessage(), e);
			}
		}
	}

	/**
	 * Instanciando o DAO
	 * 
	 * @throws ApplicationException
	 */
	public ArrayList<ProdutoPO> filtrar(ProdutoPO po) throws ApplicationException {
		try {
			DAO.abrirSessao();

			ArrayList<AbstractPO> encontrados = DAO.filtrar(po);

			/**
			 * Processo realizado com o intuito de especialisar todos os
			 * elementos do ArrayList<AbstractPO> para o ArrayList<ContatoPO>
			 */
			ArrayList<ProdutoPO> pos = new ArrayList<>();
			for (AbstractPO abstractCorrente : encontrados) {
				pos.add((ProdutoPO) abstractCorrente);
			}
			return pos;
		} catch (SessaoException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (FiltrarException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException(
					"Erro desconhecido, chame o suporte! (014 9.9678-9454)", e);
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
				throw new ApplicationException(e.getMessage(), e);
			}
		}
	}
}




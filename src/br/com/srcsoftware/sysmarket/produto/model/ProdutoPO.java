package br.com.srcsoftware.sysmarket.produto.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;

@Entity
@Table(name = "produtos", schema = "sysmarket")
public class ProdutoPO extends AbstractPO {
	@Column(name = "nome", nullable = false, length = 30)
	private String nome;

	@Column(name = "preco", nullable = false, length = 30)
	private BigDecimal preco;

	@Column(name = "quantidade", nullable = false, length = 30)
	private BigDecimal quantidade;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "codigo_unidadeDeMedida", nullable = false)
	private UnidadeDeMedidaPO unidadeDeMedida;

	public ProdutoPO() {
	}
	
	public ProdutoPO(String nome, BigDecimal preco, BigDecimal quantidade, UnidadeDeMedidaPO unidadeDeMedida) {
		setNome(nome);
		setPreco(preco);
		setQuantidade(quantidade);
		setUnidadeDeMedida(unidadeDeMedida);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public UnidadeDeMedidaPO getUnidadeDeMedida() {
		return unidadeDeMedida;
	}

	public void setUnidadeDeMedida(UnidadeDeMedidaPO unidadeDeMedida) {
		this.unidadeDeMedida = unidadeDeMedida;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((preco == null) ? 0 : preco.hashCode());
		result = prime * result
				+ ((quantidade == null) ? 0 : quantidade.hashCode());
		result = prime * result
				+ ((unidadeDeMedida == null) ? 0 : unidadeDeMedida.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoPO other = (ProdutoPO) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (preco == null) {
			if (other.preco != null)
				return false;
		} else if (!preco.equals(other.preco))
			return false;
		if (quantidade == null) {
			if (other.quantidade != null)
				return false;
		} else if (!quantidade.equals(other.quantidade))
			return false;
		if (unidadeDeMedida == null) {
			if (other.unidadeDeMedida != null)
				return false;
		} else if (!unidadeDeMedida.equals(other.unidadeDeMedida))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProdutoPO [getNome()=" + getNome() + ", getPreco()="
				+ getPreco() + ", getQuantidade()=" + getQuantidade()
				+ ", getUnidadeDeMedida()=" + getUnidadeDeMedida()
				+ ", hashCode()=" + hashCode() + "]";
	}

}

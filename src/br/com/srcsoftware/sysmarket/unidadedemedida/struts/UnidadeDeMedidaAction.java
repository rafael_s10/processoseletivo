package br.com.srcsoftware.sysmarket.unidadedemedida.struts;

import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.unidadedemedida.controller.UnidadeDeMedidaFacade;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.utilidades.Messages;

public final class UnidadeDeMedidaAction extends DispatchAction {
	public ActionForward abrirCadastro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/** fazendo o casting do form */
		UnidadeDeMedidaForm meuForm = (UnidadeDeMedidaForm) form;
		meuForm.limparCampos();

		return mapping.findForward("unidadeDeMedida_formulario");
	}

	public ActionForward inserir(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		UnidadeDeMedidaForm meuForm = (UnidadeDeMedidaForm) form;
		try {
			UnidadeDeMedidaFacade.getInstance().inserir(meuForm.montarPO());
			meuForm.limparCampos();
			this.addMessages(request, Messages.createMessages("sucesso"));

		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {
			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));

		}

		return mapping.findForward("unidadeDeMedida_formulario");
	}

	public ActionForward alterar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		UnidadeDeMedidaForm meuForm = (UnidadeDeMedidaForm) form;
		try {
			UnidadeDeMedidaFacade.getInstance().alterar(meuForm.montarPO());

			this.addMessages(request, Messages.createMessages("sucesso"));
		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {

			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));
		}

		return mapping.findForward("unidadeDeMedida_formulario");
	}

	public ActionForward excluir(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		UnidadeDeMedidaForm meuForm = (UnidadeDeMedidaForm) form;
		try {
			UnidadeDeMedidaFacade.getInstance().excluir(meuForm.montarPO());
			meuForm.limparCampos();

			this.addMessages(request, Messages.createMessages("sucesso"));
		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {

			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));
		}

		return mapping.findForward("unidadeDeMedida_formulario");
	}

	public ActionForward novo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/** fazendo o casting do form */
		UnidadeDeMedidaForm meuForm = (UnidadeDeMedidaForm) form;

		meuForm.limparCampos();

		return mapping.findForward("unidadeDeMedida_formulario");
	}

	public ActionForward selecionar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		UnidadeDeMedidaForm meuForm = (UnidadeDeMedidaForm) form;
		try {
			ArrayList<UnidadeDeMedidaPO> encontrados = UnidadeDeMedidaFacade
					.getInstance().filtrar(meuForm.montarPO());
			UnidadeDeMedidaPO selecionado = encontrados.get(0);
			meuForm.preencherUnidade(selecionado);

		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {

			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));
		}

		return mapping.findForward("unidadeDeMedida_formulario");
	}

	public ActionForward filtrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/** fazendo o casting do form */
		UnidadeDeMedidaForm meuForm = (UnidadeDeMedidaForm) form;

		try {
			ArrayList<UnidadeDeMedidaPO> encontrados = UnidadeDeMedidaFacade
					.getInstance().filtrar(meuForm.montarPO());

			meuForm.setListaUnidade(encontrados);

		} catch (ApplicationException e) {
			this.addMessages(
					request,
					Messages.createMessages("falha",
							new String[] { e.getMessage() }));
			e.printStackTrace();
		} catch (ParseException e) {

			this.addMessages(request, Messages.createMessages("falha",
					new String[] { "Erro ao converter dados do formulario" }));
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.addMessages(request, Messages.createMessages(
					"erro.desconhecido", new String[] { e.getMessage() }));
		}

		return mapping.findForward("unidadeDeMedida_tabela");
	}
}

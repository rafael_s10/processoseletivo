package br.com.srcsoftware.sysmarket.unidadedemedida.struts;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

/**
 * Classe respons�vel por conter um atributo para cada campo na tela(JSP). Todas
 * as altera��es que quiser fazer nos campos da tela, dever�o ser feitas no
 * FORM. Isso porque a tela � carregada com base numa classe FORM, sendo assim,
 * tudo que fizer na classe FORM ser� refletido nos campos da tela.
 * 
 * 
 * 
 * 
 * @author Rafael Guimaraes Santos <rafael_s10@live.com>
 * @since 23/01/2015 09:08:47
 * @version 1.0
 */

public final class UnidadeDeMedidaForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2798364118261230978L;
	/** Declarando os atributos que representarao os campos da tela (jsp) */

	private String codigo;
	private String nome;
	private String sigla;

	ArrayList<UnidadeDeMedidaPO> listaUnidade = new ArrayList<UnidadeDeMedidaPO>();

	public ArrayList<UnidadeDeMedidaPO> getListaUnidade() {
		return listaUnidade;
	}

	public void setListaAluno(ArrayList<UnidadeDeMedidaPO> listaUnidade) {
		this.listaUnidade = listaUnidade;
	}

	/**
	 * Implementa��o dos GETs e SETs. A��o obrigat�ria pois o Struts se comunica
	 * com esta classe FORM atrav�s deles. PS: Isso pelo fato de os atributos
	 * serem private e estarem encapsulado.
	 **/

	

	

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setListaUnidade(ArrayList<UnidadeDeMedidaPO> listaUnidade) {
		this.listaUnidade = listaUnidade;
	}

	public void preencherUnidade(UnidadeDeMedidaPO poEncontrado)throws ParseException, java.text.ParseException {
		this.setCodigo(poEncontrado.getCodigo().toString());
		this.setNome(poEncontrado.getNome());
		this.setSigla(poEncontrado.getSigla());
	}

	public UnidadeDeMedidaPO montarPO() throws ParseException,
			java.text.ParseException {
		UnidadeDeMedidaPO po = new UnidadeDeMedidaPO();

		if (!Utilidades.isNuloOuVazio(this.getCodigo())) {
			po.setCodigo(Long.valueOf(this.getCodigo()));

		}

		po.setNome(this.getNome());
		po.setSigla(this.getSigla());
		return po;
	}

	public void limparCampos() {
		setCodigo("");
		setNome("");
		setSigla("");
		
	}

}

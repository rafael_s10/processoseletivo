package br.com.srcsoftware.sysmarket.unidadedemedida.controller;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaSERVICE;

public class UnidadeDeMedidaFacade {
	/**
	 * Atributo responsável por possibilitar o acesso da Camada Controller a
	 * Camada MODEL
	 */
	private final UnidadeDeMedidaSERVICE SERVICE;

	// ********************************************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************************************** \\
	private static UnidadeDeMedidaFacade instance;

	private UnidadeDeMedidaFacade() {
		SERVICE = new UnidadeDeMedidaSERVICE();
	}

	public static UnidadeDeMedidaFacade getInstance() {
		if (instance == null) {
			instance = new UnidadeDeMedidaFacade();
		}
		return instance;
	}

	// ********************* FIM ******************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************* FIM ******************** \\

	public void inserir(UnidadeDeMedidaPO po) throws ApplicationException,
			InserirException {
		SERVICE.inserir(po);
	}

	public void alterar(UnidadeDeMedidaPO po) throws ApplicationException {
		SERVICE.alterar(po);
	}

	public void excluir(UnidadeDeMedidaPO po) throws ApplicationException {
		SERVICE.excluir(po);
	}

	public ArrayList<UnidadeDeMedidaPO> filtrar(UnidadeDeMedidaPO po) throws ApplicationException {
		return SERVICE.filtrar(po);
	}
}




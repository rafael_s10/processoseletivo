package br.com.srcsoftware.sysmarket.unidadedemedida.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;

@Entity
@Table(name = "unidades_de_medidas", schema = "sysmarket")
public class UnidadeDeMedidaPO extends AbstractPO {

	@Column(name = "nome", nullable = false, length = 30)
	private String nome;
	
	@Column(name = "sigla", nullable = false, length = 15)
	private String sigla;
	
	public UnidadeDeMedidaPO() {}
	
	public UnidadeDeMedidaPO(String nome, String sigla){
		setNome(nome);
		setSigla(sigla);
		
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((sigla == null) ? 0 : sigla.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeDeMedidaPO other = (UnidadeDeMedidaPO) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (sigla == null) {
			if (other.sigla != null)
				return false;
		} else if (!sigla.equals(other.sigla))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UnidadeDeMedidaPO [getNome()=" + getNome() + ", getSigla()="
				+ getSigla() + ", hashCode()=" + hashCode() + "]";
	}

}

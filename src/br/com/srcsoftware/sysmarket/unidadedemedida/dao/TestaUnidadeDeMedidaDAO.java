package br.com.srcsoftware.sysmarket.unidadedemedida.dao;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.AlterarException;
import br.com.srcsoftware.sysmarket.exceptions.CommitException;
import br.com.srcsoftware.sysmarket.exceptions.ExcluirException;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.exceptions.RollbackException;
import br.com.srcsoftware.sysmarket.exceptions.SessaoException;
import br.com.srcsoftware.sysmarket.exceptions.TransacaoException;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;

public class TestaUnidadeDeMedidaDAO {
	private static final UnidadeDeMedidaDAO DAO = new UnidadeDeMedidaDAO();

	public static void main(String[] args) {
		/** Criando um PO */

		UnidadeDeMedidaPO po = new UnidadeDeMedidaPO("sao paulo", "sp");

		/** Inserindo */
		 //inserir( po );

		/** Filtrando por ID */
		UnidadeDeMedidaPO poFiltrar = new UnidadeDeMedidaPO();
		poFiltrar.setCodigo(2L);

		ArrayList<UnidadeDeMedidaPO> encontrados = filtrar(poFiltrar);
		UnidadeDeMedidaPO selecionado = encontrados.get(0);
		System.out.println(selecionado);

		/** Alterar */
		// selecionado.setNome("gilmara");
		// alterar( selecionado );

		/** Excluindo */
		// excluir(selecionado);

	}

	/** Instanciando o DAO */
	private static void inserir(UnidadeDeMedidaPO po) {
		try {
			DAO.iniciarTransacao();

			DAO.inserir(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (SessaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();

		} catch (InserirException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (CommitException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
			}
		}

	}

	/** Instanciando o DAO */
	private static void alterar(UnidadeDeMedidaPO po) {
		try {
			DAO.iniciarTransacao();

			DAO.alterar(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (SessaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();

		} catch (CommitException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (AlterarException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
			}
		}

	}

	/** Instanciando o DAO */
	private static void excluir(UnidadeDeMedidaPO po) {
		try {
			DAO.iniciarTransacao();

			DAO.excluir(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (SessaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();

		} catch (CommitException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (ExcluirException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
			}
		}
	}

	/** Instanciando o DAO */
	private static ArrayList<UnidadeDeMedidaPO> filtrar(UnidadeDeMedidaPO po) {
		try {
			DAO.abrirSessao();

			ArrayList<AbstractPO> encontrados = DAO.filtrar(po);

			/**
			 * Processo realizado com o intuito de especialisar todos os
			 * elementos do ArrayList<AbstractPO> para o ArrayList<ContatoPO>
			 */
			ArrayList<UnidadeDeMedidaPO> pos = new ArrayList<>();
			for (AbstractPO abstractCorrente : encontrados) {
				pos.add((UnidadeDeMedidaPO) abstractCorrente);
			}
			return pos;
		} catch (SessaoException e) {
			e.printStackTrace();
		} catch (FiltrarException e) {
			e.printStackTrace();
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}

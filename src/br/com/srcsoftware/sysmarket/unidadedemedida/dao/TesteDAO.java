package br.com.srcsoftware.sysmarket.unidadedemedida.dao;

import java.util.ArrayList;

import org.hibernate.Query;

import br.com.srcsoftware.sysmarket.abstracts.AbstractDAO;
import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

public class TesteDAO extends AbstractDAO {

	@Override
	public ArrayList<AbstractPO> filtrar(AbstractPO abstractPO)	throws FiltrarException {
		if(Utilidades.isNuloOuVazio(abstractPO)){
			AbstractPO po = new UnidadeDeMedidaPO();
		}
		
		UnidadeDeMedidaPO po = null;
		
		if(abstractPO instanceof UnidadeDeMedidaPO){
			abstractPO = (UnidadeDeMedidaPO) abstractPO;
		}else{
			throw new FiltrarException("erro" + abstractPO.getClass());
		}
		
		
		
		try{
			final StringBuffer Hql = new StringBuffer();
			Hql.append("SELECT unidade FROM UnidadePO unidade");
			Hql.append("");
			Hql.append("WHERE 1=1");
			
			montarHqlPrincipal(Hql, po);
			
		
			Query query = sessaoCorrente.createQuery(Hql.toString());
			
			preencherHqlCoringaPrincipal(query, po);
			
			return (ArrayList<AbstractPO>) query.list() ;
		}catch(Exception e){
			throw new FiltrarException("erro", e);
		}
	}

	private void montarHqlPrincipal(StringBuffer Hql, UnidadeDeMedidaPO unidade) {
		if (Utilidades.isNuloOuVazio(unidade.getCodigo())) {
			Hql.append("");
			Hql.append("AND unidade.codigo = :codigoParam");

		}

	}
	private void preencherHqlCoringaPrincipal(Query query, UnidadeDeMedidaPO unidade){
		if(Utilidades.isNuloOuVazio(unidade.getCodigo())){
			query.setLong("codigoParam", unidade.getCodigo());
		}
		
	}

}

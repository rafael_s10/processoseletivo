package br.com.srcsoftware.sysmarket.unidadedemedida.dao;

import java.util.ArrayList;

import org.hibernate.Query;

import br.com.srcsoftware.sysmarket.abstracts.AbstractDAO;
import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

public class Testa extends AbstractDAO{

	@Override
	public ArrayList<AbstractPO> filtrar(AbstractPO abstractPO)
			throws FiltrarException {
		if(Utilidades.isNuloOuVazio(abstractPO)){
			abstractPO = new UnidadeDeMedidaPO();
		}
		UnidadeDeMedidaPO po = null;
		
		if(abstractPO instanceof UnidadeDeMedidaPO){
			abstractPO = (UnidadeDeMedidaPO) abstractPO;
		}else{
			throw new FiltrarException("erro" + abstractPO.getClass());
		}
		
		try{
			final StringBuffer Hql = new StringBuffer();
			Hql.append("SELECT unidade FROM UnidadeDeMedidaPO unidade");
			Hql.append("");
			Hql.append("where 1=1");
			
			montarHqlPrincipal(Hql,po);
			
			Query query = sessaoCorrente.createQuery(Hql.toString());
			
			preencherCoringasHqlPrincipal(query, po);
			
			
			
			return (ArrayList<AbstractPO>) query.list();
		}catch(Exception e){
			throw new FiltrarException("erro", e);
			
		}
		
	}
	private void montarHqlPrincipal(StringBuffer Hql, UnidadeDeMedidaPO unidade){
		if(Utilidades.isNuloOuVazio(unidade.getCodigo())){
			Hql.append("");
			Hql.append("And unidade.codigo = :codigoParam");
			
		}
		
		
		
	}
	private void preencherCoringasHqlPrincipal(Query query, UnidadeDeMedidaPO unidade){
		if(Utilidades.isNuloOuVazio(unidade.getCodigo())){
			query.setLong("codigoParam", unidade.getCodigo());
			
		}
		
	}

}

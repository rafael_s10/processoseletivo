package br.com.srcsoftware.sysmarket.unidadedemedida.dao;

import java.util.ArrayList;

import org.hibernate.Query;

import br.com.srcsoftware.sysmarket.abstracts.AbstractDAO;
import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

public final class UnidadeDeMedidaDAO extends AbstractDAO {

	@Override
	public ArrayList<AbstractPO> filtrar(AbstractPO abstractPO)throws FiltrarException {

		if (Utilidades.isNuloOuVazio(abstractPO)) {
			abstractPO = new UnidadeDeMedidaPO();
		}

		UnidadeDeMedidaPO po = null;

		/* Verificando se o abstractPO � do tipo ContatoPO */
		if (abstractPO instanceof UnidadeDeMedidaPO) {
			po = (UnidadeDeMedidaPO) abstractPO;
		} else {throw new FiltrarException("Objeto informado n�o condiz com o contexto: "+ abstractPO.getClass());
		}

		try {
			// Criando uma HQL(Hibernate Query Language)
			final StringBuffer HQL = new StringBuffer(); 
			HQL.append("SELECT unidade FROM UnidadeDeMedidaPO unidade");
			HQL.append(" ");
			HQL.append("WHERE 1=1");

			montarHQLPrincipal(HQL, po);

			/*
			 * Criando e inicializando uma variavel respons�vel por criar uma
			 * Query com base na nossa HQL criada acima deixando-a preparada
			 * para o Hibernate executa-la.
			 */
			Query query = sessaoCorrente.createQuery(HQL.toString()); 

			/* Preenchendo os coringas da HQL */
			preecherCoringasHQLPrincipal(query, po);

			/*
			 * Executando a consulta list() = Consulta mais que um
			 * uniqueResult() = Consulta apenas 1 registro. Caso a HQL resulte
			 * em mais de um, uma excess�o ser� lan�ada.
			 */
			return (ArrayList<AbstractPO>) query.list();
		} catch (Exception e) {
			throw new FiltrarException("Erro ao filtrar", e);
		}

	}

	private void montarHQLPrincipal(StringBuffer HQL, UnidadeDeMedidaPO unidade) {

		if (!Utilidades.isNuloOuVazio(unidade.getCodigo())) {
			HQL.append(" ");
			HQL.append(" AND unidade.codigo = :codigoParam");
		}

		if (!Utilidades.isNuloOuVazio(unidade.getNome())) {
			HQL.append(" ");
			HQL.append(" AND unidade.nome = :nomeParam");
		}

		if (!Utilidades.isNuloOuVazio(unidade.getSigla())) {
			HQL.append(" ");
			HQL.append(" AND unidade.sigla = :siglaParam");
		}

	}

	private void preecherCoringasHQLPrincipal(Query query,UnidadeDeMedidaPO unidade) {

		if (!Utilidades.isNuloOuVazio(unidade.getCodigo())) {
			query.setLong("codigoParam", unidade.getCodigo());
		}

		if (!Utilidades.isNuloOuVazio(unidade.getNome())) {
			query.setString("nomeParam", unidade.getNome());
		}

		if (!Utilidades.isNuloOuVazio(unidade.getSigla())) {
			query.setString("tipoParam", unidade.getSigla());
		}

	}

}

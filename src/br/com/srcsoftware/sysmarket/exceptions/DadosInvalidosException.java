package br.com.srcsoftware.sysmarket.exceptions;

public final class DadosInvalidosException extends Exception{
	public DadosInvalidosException( String message ){
		super( message );
	}

	public DadosInvalidosException( String message, Throwable cause ){
		super( message, cause );
	}
}

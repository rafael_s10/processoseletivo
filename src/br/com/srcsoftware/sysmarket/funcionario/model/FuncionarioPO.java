package br.com.srcsoftware.sysmarket.funcionario.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;

@Entity
@Table(name = "funcionarios", schema = "sysmarket_t8")
public class FuncionarioPO extends AbstractPO {

	@Column(name = "nome", nullable = false, length = 30)
	private String nome;

	@Column(name = "setor", nullable = false, length = 15)
	private String setor;

	public FuncionarioPO() {
	}

	public FuncionarioPO(String nome, String setor){
		setNome(nome);
		setSetor(setor);
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((setor == null) ? 0 : setor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FuncionarioPO other = (FuncionarioPO) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (setor == null) {
			if (other.setor != null)
				return false;
		} else if (!setor.equals(other.setor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FuncionarioPO [getNome()=" + getNome() + ", getSetor()="
				+ getSetor() + ", hashCode()=" + hashCode() + "]";
	}

}
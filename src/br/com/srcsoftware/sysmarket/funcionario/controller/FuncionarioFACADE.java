package br.com.srcsoftware.sysmarket.funcionario.controller;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.funcionario.model.FuncionarioPO;
import br.com.srcsoftware.sysmarket.funcionario.model.FuncionarioSERVICE;

public class FuncionarioFACADE {
	/**
	 * Atributo responsável por possibilitar o acesso da Camada Controller a
	 * Camada MODEL
	 */
	private final FuncionarioSERVICE SERVICE;

	// ********************************************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************************************** \\
	private static FuncionarioFACADE instance;

	private FuncionarioFACADE() {
		SERVICE = new FuncionarioSERVICE();
	}

	public static FuncionarioFACADE getInstance() {
		if (instance == null) {
			instance = new FuncionarioFACADE();
		}
		return instance;
	}

	// ********************* FIM ******************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************* FIM ******************** \\

	public void inserir(FuncionarioPO po) throws ApplicationException,
			InserirException {
		SERVICE.inserir(po);
	}

	public void alterar(FuncionarioPO po) throws ApplicationException {
		SERVICE.alterar(po);
	}

	public void excluir(FuncionarioPO po) throws ApplicationException {
		SERVICE.excluir(po);
	}

	public ArrayList<FuncionarioPO> filtrar(FuncionarioPO po)
			throws ApplicationException {
		return SERVICE.filtrar(po);
	}
}

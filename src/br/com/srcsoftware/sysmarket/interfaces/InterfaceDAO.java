package br.com.srcsoftware.sysmarket.interfaces;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.AlterarException;
import br.com.srcsoftware.sysmarket.exceptions.CommitException;
import br.com.srcsoftware.sysmarket.exceptions.ExcluirException;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.exceptions.RollbackException;
import br.com.srcsoftware.sysmarket.exceptions.SessaoException;
import br.com.srcsoftware.sysmarket.exceptions.TransacaoException;

public interface InterfaceDAO {

	void abrirSessao() throws SessaoException;

	void iniciarTransacao() throws TransacaoException, SessaoException;

	void fecharSessao() throws SessaoException;

	void commitTransacao() throws CommitException;

	void rollbackTransacao() throws RollbackException;

	/**
	 * M�todo respons�vel por inserir um Objeto na base de dados com o aux�lio
	 * da framework Hibernate.
	 * 
	 * @param Object
	 *            obj - Objeto que sera persistido.
	 * @return Long - Id do objeto recem persistido.
	 * 
	 * @author Lucas Pedro Bermejo <lucas_bermejo@hotmail.com>
	 * @since 09/01/2015 08:44:09
	 * @version 1.0
	 * @throws Exception
	 */

	/**
	 * M�todo respons�vel por alterar um Objeto na base de dados com o aux�lio
	 * da framework Hibernate.
	 * 
	 * @param AbstractDTO
	 *            obj - Objeto contendo os dados a serem alterados.
	 * @throws Exception
	 * 
	 * @author Lucas Pedro Bermejo <lucas_bermejo@hotmail.com>
	 * @since 09/01/2015 08:44:09
	 * @version 1.0
	 */
	void alterar(AbstractPO po) throws AlterarException;

	/**
	 * M�todo respons�vel por excluir um Objeto na base de dados com o aux�lio
	 * da framework Hibernate.
	 * 
	 * @param AbstractDTO
	 *            obj - Objeto contendo os dados a serem excluirdos.
	 * @throws Exception
	 * 
	 * @author Lucas Pedro Bermejo <lucas_bermejo@hotmail.com>
	 * @since 09/01/2015 08:44:09
	 * @version 1.0
	 */
	void excluir(AbstractPO po) throws ExcluirException;

	Long inserir(AbstractPO po) throws AlterarException, InserirException;

	ArrayList<AbstractPO> filtrar(AbstractPO abstractPO)
			throws FiltrarException;
}

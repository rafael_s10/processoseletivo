package br.com.srcsoftware.sysmarket.mercado.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.estacionamento.model.EstacionamentoPO;
import br.com.srcsoftware.sysmarket.funcionario.model.FuncionarioPO;
import br.com.srcsoftware.sysmarket.produto.model.ProdutoPO;

@Entity
@Table(name = "mercados", schema = "sysmarket")
public class MercadoPO extends AbstractPO {
	@Column(name = "nome", nullable = false, length = 30)
	private String nome;

	@Column(name = "endereco", nullable = false, length = 30)
	private String endereco;

	@OneToMany( fetch = FetchType.EAGER, cascade = CascadeType.ALL )
	@JoinColumn( name = "id_mercado", nullable = false )
	private Set< FuncionarioPO > funcionarios = new HashSet< FuncionarioPO >();

	@ManyToMany( fetch = FetchType.EAGER, cascade = CascadeType.ALL )
	@JoinTable( name = "produtos_mercado", joinColumns = @JoinColumn( name = "id_mercado" ), inverseJoinColumns = @JoinColumn( name = "id_produto" ), schema = "sysmarket_t8" )
	private Set< ProdutoPO > produtos = new HashSet< ProdutoPO >();

	@OneToOne(mappedBy = "mercado", fetch = FetchType.EAGER)
	private EstacionamentoPO estacionamento;

	public MercadoPO() {
	}

	public MercadoPO(String nome, String endereco, Set<ProdutoPO> produtos, Set<FuncionarioPO> funcionarios) {
		setNome(nome);
		setEndereco(endereco);
		setFuncionarios(funcionarios);
		setProdutos(produtos);		

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Set<FuncionarioPO> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Set<FuncionarioPO> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public Set<ProdutoPO> getProdutos() {
		return produtos;
	}

	public void setProdutos(Set<ProdutoPO> produtos) {
		this.produtos = produtos;
	}

	public EstacionamentoPO getEstacionamento() {
		return estacionamento;
	}

	public void setEstacionamento(EstacionamentoPO estacionamento) {
		this.estacionamento = estacionamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result
				+ ((estacionamento == null) ? 0 : estacionamento.hashCode());
		result = prime * result
				+ ((funcionarios == null) ? 0 : funcionarios.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((produtos == null) ? 0 : produtos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MercadoPO other = (MercadoPO) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (estacionamento == null) {
			if (other.estacionamento != null)
				return false;
		} else if (!estacionamento.equals(other.estacionamento))
			return false;
		if (funcionarios == null) {
			if (other.funcionarios != null)
				return false;
		} else if (!funcionarios.equals(other.funcionarios))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (produtos == null) {
			if (other.produtos != null)
				return false;
		} else if (!produtos.equals(other.produtos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MercadoPO [getNome()=" + getNome() + ", getEndereco()="
				+ getEndereco() + ", getFuncionarios()=" + getFuncionarios()
				+ ", getProdutos()=" + getProdutos() + ", getEstacionamento()="
				+ getEstacionamento() + ", hashCode()=" + hashCode() + "]";
	}	
	
}

	
package br.com.srcsoftware.sysmarket.mercado.controller;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.exceptions.RollbackException;
import br.com.srcsoftware.sysmarket.mercado.model.MercadoPO;
import br.com.srcsoftware.sysmarket.mercado.model.MercadoSERVICE;

public class MercadoFacade {
	private final MercadoSERVICE SERVICE;

	// ********************************************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************************************** \\
	private static MercadoFacade instance;

	private MercadoFacade() {
		SERVICE = new MercadoSERVICE();
	}

	public static MercadoFacade getInstance() {
		if (instance == null) {
			instance = new MercadoFacade();
		}
		return instance;
	}

	// ********************* FIM ******************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************* FIM ******************** \\

	public void inserir(MercadoPO po) throws ApplicationException,
			InserirException, RollbackException {
		SERVICE.inserir(po);
	}

	public void alterar(MercadoPO po) throws ApplicationException, RollbackException {
		SERVICE.alterar(po);
	}

	public void excluir(MercadoPO po) throws ApplicationException, RollbackException {
		SERVICE.excluir(po);
	}

	public ArrayList<MercadoPO> filtrar(MercadoPO po) throws ApplicationException {
		return SERVICE.filtrar(po);
	}
}







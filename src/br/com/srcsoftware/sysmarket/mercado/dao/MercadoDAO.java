package br.com.srcsoftware.sysmarket.mercado.dao;

import java.util.ArrayList;
import java.util.Set;

import org.hibernate.Query;

import br.com.srcsoftware.sysmarket.abstracts.AbstractDAO;
import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.funcionario.model.FuncionarioPO;
import br.com.srcsoftware.sysmarket.mercado.model.MercadoPO;
import br.com.srcsoftware.sysmarket.produto.model.ProdutoPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

public class MercadoDAO extends AbstractDAO {
	@Override
	public ArrayList<AbstractPO> filtrar(AbstractPO abstractPO)
			throws FiltrarException {

		if (Utilidades.isNuloOuVazio(abstractPO)) {
			abstractPO = new MercadoPO();
		}

		MercadoPO po = null;

		/* Verificando se o abstractPO � do tipo ContatoPO */
		if (abstractPO instanceof MercadoPO) {
			po = (MercadoPO) abstractPO;
		} else {
			throw new FiltrarException(
					"Objeto informado n�o condiz com o contexto: "
							+ abstractPO.getClass());
		}

		try {
			// Criando uma HQL(Hibernate Query Language)
			final StringBuffer HQL = new StringBuffer();
			HQL.append("SELECT mercado FROM MercadoPO mercado");
			HQL.append(" ");
			HQL.append("WHERE 1=1");

			montarHQLPrincipal(HQL, po);

			/*
			 * Criando e inicializando uma variavel respons�vel por criar uma
			 * Query com base na nossa HQL criada acima deixando-a preparada
			 * para o Hibernate executa-la.
			 */
			Query query = sessaoCorrente.createQuery(HQL.toString());

			/* Preenchendo os coringas da HQL */
			preecherCoringasHQLPrincipal(query, po);

			/*
			 * Executando a consulta list() = Consulta mais que um
			 * uniqueResult() = Consulta apenas 1 registro. Caso a HQL resulte
			 * em mais de um, uma excess�o ser� lan�ada.
			 */
			return (ArrayList<AbstractPO>) query.list();
		} catch (Exception e) {
			throw new FiltrarException("Erro ao filtrar", e);
		}

	}

	private void montarHQLPrincipal(StringBuffer HQL, MercadoPO mercado) {

		if (!Utilidades.isNuloOuVazio(mercado.getCodigo())) {
			HQL.append(" ");
			HQL.append(" AND mercado.codigo = :codigoParam");
		}

		if (!Utilidades.isNuloOuVazio(mercado.getNome())) {
			HQL.append(" ");
			HQL.append(" AND mercado.nome = :nomeParam");
		}

		if (!Utilidades.isNuloOuVazio(mercado.getEndereco())) {
			HQL.append(" ");
			HQL.append(" AND mercado.endereco = :mercadoParam");
		}

		/** Verificando se a funcionario foi enviado */
		if (!Utilidades.isNuloOuVazio(mercado.getProdutos())) {
			montarHQLProduto(HQL, mercado.getProdutos());
		}

		HQL.append(" ");
		HQL.append("order by mercado.nome");

		/** Verificando se a produtos foi enviado */
		if (!Utilidades.isNuloOuVazio(mercado.getFuncionarios())) {
			montarHQLFuncionarios(HQL, mercado.getFuncionarios());
		}

		HQL.append(" ");
		HQL.append("order by funcionario.nome");
	}

	private void montarHQLProduto(StringBuffer HQL, Set<ProdutoPO> produtos) {
		
		for(ProdutoPO produtosCorrente : produtos ){
		
		if (!Utilidades.isNuloOuVazio(produtosCorrente.getCodigo())) {
			HQL.append(" ");
			HQL.append(" AND mercado.produtos.nome = :nomeParam");
		}

		if (!Utilidades.isNuloOuVazio(produtosCorrente.getPreco())) {
			HQL.append(" ");
			HQL.append(" AND mercado.produtoss.preco = :precoParam");
		}

		if (!Utilidades.isNuloOuVazio(produtosCorrente.getQuantidade())) {
			HQL.append(" ");
			HQL.append(" AND mercado.produtos.quantidade = :quantidadeParam");
		}
		}

	}

	private void montarHQLFuncionarios(StringBuffer HQL,	Set<FuncionarioPO> funcionarios) {
		
		for(FuncionarioPO funcionariosCorrente : funcionarios){
			
		
		
		if (!Utilidades.isNuloOuVazio(funcionariosCorrente.getNome())) {
			HQL.append(" ");
			HQL.append(" AND mercado.funcionarios.nome = :nomeParam");
		}

		if (!Utilidades.isNuloOuVazio(funcionariosCorrente.getSetor())) {
			HQL.append(" ");
			HQL.append(" AND mercado.funcionario.setor = :setorParam");
		}

		if (!Utilidades.isNuloOuVazio(funcionariosCorrente.getCodigo())) {
			HQL.append(" ");
			HQL.append(" AND mercado.funcionario.codigo = :codigoParam");
		}
		}

	}

	private void preecherCoringasHQLPrincipal(Query query, MercadoPO mercado) {

		if (!Utilidades.isNuloOuVazio(mercado.getCodigo())) {
			query.setLong("codigoParam", mercado.getCodigo());
		}

		if (!Utilidades.isNuloOuVazio(mercado.getNome())) {
			query.setString("nomeParam", mercado.getNome());
		}

		if (!Utilidades.isNuloOuVazio(mercado.getEndereco())) {
			query.setString("precoParam", mercado.getEndereco());
		}

		/** Verificando se a produtos foi enviado */
		if (!Utilidades.isNuloOuVazio(mercado.getProdutos())) {
			preecherCoringasHQLProduto(query, mercado.getProdutos());
		}

		/** Verificando se a produtos foi enviado */
		if (!Utilidades.isNuloOuVazio(mercado.getFuncionarios())) {
			preecherCoringasHQLFuncionarios(query, mercado.getFuncionarios());
		}
	}

	private void preecherCoringasHQLProduto(Query query, Set<ProdutoPO> produtos) {
		for(ProdutoPO produtosCorrente : produtos){
		if (!Utilidades.isNuloOuVazio(produtosCorrente.getNome())) {
			query.setString("nomeParam", produtosCorrente.getNome());
		}

		if (!Utilidades.isNuloOuVazio(produtosCorrente.getPreco())) {
			query.setBigDecimal("precoParam", produtosCorrente.getPreco());
		}
		if (!Utilidades.isNuloOuVazio(produtosCorrente.getQuantidade())) {
			query.setBigDecimal("quantidadeParam", produtosCorrente.getQuantidade());
		}
		}

	}

	private void preecherCoringasHQLFuncionarios(Query query,Set<FuncionarioPO> funcionarios) {
		for(FuncionarioPO funcionariosCorrente : funcionarios){
		if (!Utilidades.isNuloOuVazio(funcionariosCorrente.getNome())) {
			query.setString("nomeParam", funcionariosCorrente.getNome());
		}

		if (!Utilidades.isNuloOuVazio(funcionariosCorrente.getSetor())) {
			query.setString("setorParam", funcionariosCorrente.getSetor());
		}
		}

	}
}

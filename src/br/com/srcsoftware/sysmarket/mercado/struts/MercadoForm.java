package br.com.srcsoftware.sysmarket.mercado.struts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.apache.struts.action.ActionForm;
import org.apache.struts.util.LabelValueBean;

import br.com.srcsoftware.sysmarket.funcionario.controller.FuncionarioFACADE;
import br.com.srcsoftware.sysmarket.funcionario.model.FuncionarioPO;
import br.com.srcsoftware.sysmarket.mercado.model.MercadoPO;
import br.com.srcsoftware.sysmarket.produto.model.ProdutoPO;
import br.com.srcsoftware.sysmarket.unidadedemedida.model.UnidadeDeMedidaPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

/**
 * Classe respons�vel por conter um atributo para cada campo na tela(JSP). Todas
 * as altera��es que quiser fazer nos campos da tela, dever�o ser feitas no
 * FORM. Isso porque a tela � carregada com base numa classe FORM, sendo assim,
 * tudo que fizer na classe FORM ser� refletido nos campos da tela.
 * 
 * 
 * 
 * 
 * @author Rafael Guimaraes Santos <rafael_s10@live.com>
 * @since 23/01/2015 09:08:47
 * @version 1.0
 */

public final class MercadoForm extends ActionForm {

	/** Declarando os atributos que representarao os campos da tela (jsp) */
	private String codigo;
	private String nome;
	private String endereco;

	// atributos relacionados a cHAVE ESTRANGEIRA
	private String nomeFuncionario;
	private String setor;
	private String codigoFuncionario;

	// atributos relacionados a cHAVE ESTRANGEIRA
	private String nomeProduto;
	private String preco;
	private String quantidade;
	private String codigoProduto;

	ArrayList<MercadoPO> listaMercado = new ArrayList<MercadoPO>();
	
	ArrayList<FuncionarioPO> listaFuncionarios = new ArrayList<FuncionarioPO>();
	
	ArrayList<ProdutoPO> listaProdutos = new ArrayList<ProdutoPO>();
	

	
	public ArrayList<FuncionarioPO> getListaFuncionarios() {
		return listaFuncionarios;
	}

	public void setListaFuncionarios(ArrayList<FuncionarioPO> listaFuncionarios) {
		this.listaFuncionarios = listaFuncionarios;
	}

	public ArrayList<ProdutoPO> getListaProdutos() {
		return listaProdutos;
	}

	public void setListaProdutos(ArrayList<ProdutoPO> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getCodigoFuncionario() {
		return codigoFuncionario;
	}

	public void setCodigoFuncionario(String codigoFuncionario) {
		this.codigoFuncionario = codigoFuncionario;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public String getPreco() {
		return preco;
	}

	public void setPreco(String preco) {
		this.preco = preco;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public String getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(String codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public ArrayList<MercadoPO> getListaMercado() {
		return listaMercado;
	}

	public void setListaMercado(ArrayList<MercadoPO> listaMercado) {
		this.listaMercado = listaMercado;
	}

	public ArrayList<LabelValueBean> getComboFuncionario() throws Throwable {
		ArrayList<LabelValueBean> lista = new ArrayList<LabelValueBean>();

		try {
			ArrayList<FuncionarioPO> objetosEncontrados = FuncionarioFACADE
					.getInstance().filtrar(null);
			Iterator<FuncionarioPO> setIter = objetosEncontrados.iterator();
			LabelValueBean lvb = new LabelValueBean();
			lvb.setLabel("Selecione...");
			lvb.setValue(null);
			lista.add(lvb);

			while (setIter.hasNext()) {
				FuncionarioPO funcionario = (FuncionarioPO) setIter.next();
				lvb = new LabelValueBean();

				lvb.setLabel(funcionario.getNome() + "-"+ funcionario.getCodigo());
				lvb.setValue(funcionario.getCodigo().toString());

				lista.add(lvb);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	

	public void preencherMercado(MercadoPO poEncontrado) throws ParseException,java.text.ParseException {
		this.setCodigo(poEncontrado.getCodigo().toString());
		this.setEndereco(poEncontrado.getEndereco());
		this.setNome(poEncontrado.getNome());	
	}		
	
	public void preencherProduto(Set<ProdutoPO> produtos){	
		for(ProdutoPO produtoCorrente: produtos){
		this.setNomeProduto(produtoCorrente.getNome());
		this.setPreco(produtoCorrente.getPreco().toString());
		this.setQuantidade(produtoCorrente.getQuantidade().toString());
		}		
	}
	
	public void preencherFuncionario(Set<FuncionarioPO> funcionarios){	
		for(FuncionarioPO funcionarioCorrente: funcionarios){
		this.setNomeFuncionario(funcionarioCorrente.getNome());
		this.setSetor(funcionarioCorrente.getSetor());
		this.setCodigoFuncionario(funcionarioCorrente.getCodigo().toString());
		}
		
	}

	public MercadoPO montarPO() throws ParseException, java.text.ParseException {
		MercadoPO po = new MercadoPO();

		if (!Utilidades.isNuloOuVazio(this.getCodigo())) {
			po.setCodigo(Long.valueOf(this.getCodigo()));
		}
		po.setNome(this.getNome());
		po.setEndereco(this.getEndereco());

		/** montando Produto */
		po.setProdutos(new ProdutoPO());
		po.setProdutos(new Set<ProdutoPO>);

		if (!Utilidades.isNuloOuVazio(getCodigoProduto())) {
			po.getProdutos().setCodigo(Long.valueOf(getCodigoProduto()));
		}

		if (!Utilidades.isNuloOuVazio(getPreco())) {
			po.getProduto().setPreco(new BigDecimal(this.getPreco()));
		}
		
		if (!Utilidades.isNuloOuVazio(getQuantidade())) {
			po.getProduto().setQuantidade(new BigDecimal(this.getQuantidade()));
		}
		po.getProduto().setNome(this.getNome());
		
		/** montando Funcionario*/
		po.setFuncionario(new FuncionarioPO());
		if (!Utilidades.isNuloOuVazio(getCodigoFuncionario())) {
			po.getFuncionarios().setCodigo(Long.valueOf(getCodigoFuncionario()));
		}
		

		po.getFuncionarios().setNome(this.getNome());
		po.getFuncionarios().setSetor(this.getSetor());
		return po;
	}

	public void limparCampos() {
		setCodigo("");
		setNome("");
		setEndereco("");		
		setCodigoFuncionario("");
		setNomeFuncionario("");
		setSetor("");		
		setCodigoProduto("");
		setPreco("");
		setQuantidade("");
		setNomeProduto("");
		
		
		

	}

}

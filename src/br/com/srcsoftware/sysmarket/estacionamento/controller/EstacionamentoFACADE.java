package br.com.srcsoftware.sysmarket.estacionamento.controller;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.estacionamento.model.EstacionamentoPO;
import br.com.srcsoftware.sysmarket.estacionamento.model.EstacionamentoSERVICE;
import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.exceptions.RollbackException;

public class EstacionamentoFACADE {
	/**
	 * Atributo responsável por possibilitar o acesso da Camada Controller a
	 * Camada MODEL
	 */
	private final EstacionamentoSERVICE SERVICE;

	// ********************************************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************************************** \\
	private static EstacionamentoFACADE instance;

	private EstacionamentoFACADE() {
		SERVICE = new EstacionamentoSERVICE();
	}

	public static EstacionamentoFACADE getInstance() {
		if (instance == null) {
			instance = new EstacionamentoFACADE();
		}
		return instance;
	}

	// ********************* FIM ******************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************* FIM ******************** \\

	public void inserir(EstacionamentoPO po) throws ApplicationException,
			InserirException, RollbackException {
		SERVICE.inserir(po);
	}

	public void alterar(EstacionamentoPO po) throws ApplicationException,
			RollbackException {
		SERVICE.alterar(po);
	}

	public void excluir(EstacionamentoPO po) throws ApplicationException,
			RollbackException {
		SERVICE.excluir(po);
	}

	public ArrayList<EstacionamentoPO> filtrar(EstacionamentoPO po)
			throws ApplicationException {
		return SERVICE.filtrar(po);
	}
}

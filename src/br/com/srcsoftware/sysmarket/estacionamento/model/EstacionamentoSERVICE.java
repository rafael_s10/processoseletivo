package br.com.srcsoftware.sysmarket.estacionamento.model;

import java.util.ArrayList;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.exceptions.AlterarException;
import br.com.srcsoftware.sysmarket.exceptions.ApplicationException;
import br.com.srcsoftware.sysmarket.exceptions.CommitException;
import br.com.srcsoftware.sysmarket.exceptions.ExcluirException;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.exceptions.RollbackException;
import br.com.srcsoftware.sysmarket.exceptions.SessaoException;
import br.com.srcsoftware.sysmarket.exceptions.TransacaoException;
import br.com.srcsoftware.sysmarket.interfaces.InterfaceDAO;
import br.com.srcsoftware.sysmarket.unidadedemedida.dao.UnidadeDeMedidaDAO;

public final class EstacionamentoSERVICE {
	private final InterfaceDAO DAO;

	public EstacionamentoSERVICE() {
		DAO = new UnidadeDeMedidaDAO();
	}

	public void inserir(EstacionamentoPO po) throws ApplicationException,
			InserirException {
		try {

			DAO.iniciarTransacao();

			DAO.inserir(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (SessaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (CommitException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (Exception e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(
					"Erro desconhecido, chame o suporte! (014 9.9678-9454)", e);
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
				throw new ApplicationException(e.getMessage(), e);
			}
		}

	}

	/**
	 * Instanciando o DAO
	 * 
	 * @throws ApplicationException
	 */
	public void alterar(EstacionamentoPO po) throws ApplicationException {
		try {

			DAO.iniciarTransacao();

			DAO.alterar(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (SessaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (CommitException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (AlterarException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (Exception e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(
					"Erro desconhecido, chame o suporte! (014 9.9678-9454)", e);
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
				throw new ApplicationException(e.getMessage(), e);
			}
		}

	}

	/**
	 * Instanciando o DAO
	 * 
	 * @throws ApplicationException
	 */
	public void excluir(EstacionamentoPO po) throws ApplicationException {
		try {
			DAO.iniciarTransacao();

			DAO.excluir(po);

			DAO.commitTransacao();

		} catch (TransacaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (SessaoException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (CommitException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (ExcluirException e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (Exception e) {
			try {
				DAO.rollbackTransacao();
			} catch (RollbackException e1) {
				e1.printStackTrace();
				throw new ApplicationException(e1.getMessage(), e);
			}
			e.printStackTrace();
			throw new ApplicationException(
					"Erro desconhecido, chame o suporte! (014 9.9678-9454)", e);
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
				throw new ApplicationException(e.getMessage(), e);
			}
		}
	}

	/**
	 * Instanciando o DAO
	 * 
	 * @throws ApplicationException
	 */
	public ArrayList<EstacionamentoPO> filtrar(EstacionamentoPO po)
			throws ApplicationException {
		try {
			DAO.abrirSessao();

			ArrayList<AbstractPO> encontrados = DAO.filtrar(po);

			/**
			 * Processo realizado com o intuito de especialisar todos os
			 * elementos do ArrayList<AbstractPO> para o ArrayList<ContatoPO>
			 */
			ArrayList<EstacionamentoPO> pos = new ArrayList<>();
			for (AbstractPO abstractCorrente : encontrados) {
				pos.add((EstacionamentoPO) abstractCorrente);
			}
			return pos;
		} catch (SessaoException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (FiltrarException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException(
					"Erro desconhecido, chame o suporte! (014 9.9678-9454)", e);
		} finally {
			try {
				DAO.fecharSessao();
			} catch (SessaoException e) {
				e.printStackTrace();
				throw new ApplicationException(e.getMessage(), e);
			}
		}
	}
}

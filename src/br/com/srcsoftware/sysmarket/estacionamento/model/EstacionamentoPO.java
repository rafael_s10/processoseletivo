package br.com.srcsoftware.sysmarket.estacionamento.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.mercado.model.MercadoPO;

@Entity
@Table(name = "estacionamentos", schema = "sysmarket_t8")
public class EstacionamentoPO extends AbstractPO {
	@Column(name = "quantidade_De_Vagas", nullable = false, length = 30)
	private Byte quantidadeDeVagas;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "codigo_mercado", nullable = false)
	private MercadoPO mercado;

	public EstacionamentoPO() {
	}

	public EstacionamentoPO( Byte quantidadeDeVagas) {
		setQuantidadeDeVagas(quantidadeDeVagas);
	}

	public Byte getQuantidadeDeVagas() {
		return quantidadeDeVagas;
	}

	public void setQuantidadeDeVagas(Byte quantidadeDeVagas) {
		this.quantidadeDeVagas = quantidadeDeVagas;
	}

	public MercadoPO getMercado() {
		return mercado;
	}

	public void setMercado(MercadoPO mercado) {
		this.mercado = mercado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((mercado == null) ? 0 : mercado.hashCode());
		result = prime
				* result
				+ ((quantidadeDeVagas == null) ? 0 : quantidadeDeVagas
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstacionamentoPO other = (EstacionamentoPO) obj;
		if (mercado == null) {
			if (other.mercado != null)
				return false;
		} else if (!mercado.equals(other.mercado))
			return false;
		if (quantidadeDeVagas == null) {
			if (other.quantidadeDeVagas != null)
				return false;
		} else if (!quantidadeDeVagas.equals(other.quantidadeDeVagas))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EstacionamentoPO [getQuantidadeDeVagas()="
				+ getQuantidadeDeVagas() + ", getMercado()=" + getMercado()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
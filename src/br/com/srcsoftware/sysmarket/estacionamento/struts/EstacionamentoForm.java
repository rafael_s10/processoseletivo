package br.com.srcsoftware.sysmarket.estacionamento.struts;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.util.LabelValueBean;

import br.com.srcsoftware.sysmarket.estacionamento.model.EstacionamentoPO;
import br.com.srcsoftware.sysmarket.mercado.controller.MercadoFacade;
import br.com.srcsoftware.sysmarket.mercado.model.MercadoPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

/**
 * Classe respons�vel por conter um atributo para cada campo na tela(JSP). Todas
 * as altera��es que quiser fazer nos campos da tela, dever�o ser feitas no
 * FORM. Isso porque a tela � carregada com base numa classe FORM, sendo assim,
 * tudo que fizer na classe FORM ser� refletido nos campos da tela.
 * 
 * 
 * 
 * 
 * @author Rafael Guimaraes Santos <rafael_s10@live.com>
 * @since 23/01/2015 09:08:47
 * @version 1.0
 */

public final class EstacionamentoForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2798364118261230978L;
	/** Declarando os atributos que representarao os campos da tela (jsp) */

	private String codigo;
	private String quantidadeDeVagas;

	// atributos relacionados a chave estrangeira
	private String codigoMercado;
	private String nome;
	private String endereco;
	ArrayList<EstacionamentoPO> listaEstacionamento = new ArrayList<EstacionamentoPO>();

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getQuantidadeDeVagas() {
		return quantidadeDeVagas;
	}

	public void setQuantidadeDeVagas(String quantidadeDeVagas) {
		this.quantidadeDeVagas = quantidadeDeVagas;
	}

	public String getCodigoMercado() {
		return codigoMercado;
	}

	public void setCodigoMercado(String codigoMercado) {
		this.codigoMercado = codigoMercado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public ArrayList<EstacionamentoPO> getListaEstacionamento() {
		return listaEstacionamento;
	}

	public void setListaEstacionamento(
			ArrayList<EstacionamentoPO> listaEstacionamento) {
		this.listaEstacionamento = listaEstacionamento;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ArrayList<LabelValueBean> getComboMercado() throws Throwable {
		ArrayList<LabelValueBean> lista = new ArrayList<LabelValueBean>();

		try {
			ArrayList<MercadoPO> objetosEncontrados = MercadoFacade
					.getInstance().filtrar(null);
			Iterator<MercadoPO> setIter = objetosEncontrados.iterator();
			LabelValueBean lvb = new LabelValueBean();
			lvb.setLabel("Selecione...");
			lvb.setValue(null);
			lista.add(lvb);

			while (setIter.hasNext()) {
				MercadoPO mercado = (MercadoPO) setIter.next();
				lvb = new LabelValueBean();

				lvb.setLabel(mercado.getNome() + "-" + mercado.getCodigo());
				lvb.setValue(mercado.getCodigo().toString());

				lista.add(lvb);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public void preencherEstacionamento(EstacionamentoPO poEncontrado)
			throws ParseException, java.text.ParseException {
		this.setCodigo(poEncontrado.getCodigo().toString());
		this.setQuantidadeDeVagas(poEncontrado.getQuantidadeDeVagas()
				.toString());
		this.setCodigoMercado(poEncontrado.getMercado().getCodigo().toString());
		this.setEndereco(poEncontrado.getMercado().getEndereco());
		this.setNome(poEncontrado.getMercado().getNome());
	}

	public EstacionamentoPO montarPO() throws ParseException,
			java.text.ParseException {
		EstacionamentoPO po = new EstacionamentoPO();

		if (!Utilidades.isNuloOuVazio(this.getCodigo())) {
			po.setCodigo(Long.valueOf(this.getCodigo()));

		}

		if (!Utilidades.isNuloOuVazio(this.getQuantidadeDeVagas())) {
			po.setQuantidadeDeVagas(Byte.valueOf(this.getQuantidadeDeVagas()));

		}

		// montar mercado
		po.setMercado(new MercadoPO());
		if (Utilidades.isNuloOuVazio(getCodigoMercado())) {
			po.getMercado().setCodigo(Long.valueOf(getCodigoMercado()));

		}
		po.getMercado().setEndereco(this.endereco);
		po.getMercado().setNome(this.nome);
		return po;
	}

	public void limparCampos() {
		setCodigo("");
		setQuantidadeDeVagas("");
		setCodigoMercado("");
		setEndereco("");
		setNome("");

	}

}

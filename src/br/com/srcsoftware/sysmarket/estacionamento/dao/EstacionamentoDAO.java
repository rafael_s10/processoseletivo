package br.com.srcsoftware.sysmarket.estacionamento.dao;

import java.util.ArrayList;

import org.hibernate.Query;

import br.com.srcsoftware.sysmarket.abstracts.AbstractDAO;
import br.com.srcsoftware.sysmarket.abstracts.AbstractPO;
import br.com.srcsoftware.sysmarket.estacionamento.model.EstacionamentoPO;
import br.com.srcsoftware.sysmarket.exceptions.FiltrarException;
import br.com.srcsoftware.sysmarket.mercado.model.MercadoPO;
import br.com.srcsoftware.sysmarket.utilidades.Utilidades;

public class EstacionamentoDAO extends AbstractDAO {
	@Override
	public ArrayList<AbstractPO> filtrar(AbstractPO abstractPO)
			throws FiltrarException {

		if (Utilidades.isNuloOuVazio(abstractPO)) {
			abstractPO = new EstacionamentoPO();
		}

		EstacionamentoPO po = null;

		/* Verificando se o abstractPO � do tipo ContatoPO */
		if (abstractPO instanceof EstacionamentoPO) {
			po = (EstacionamentoPO) abstractPO;
		} else {
			throw new FiltrarException(
					"Objeto informado n�o condiz com o contexto: "
							+ abstractPO.getClass());
		}

		try {
			// Criando uma HQL(Hibernate Query Language)
			final StringBuffer HQL = new StringBuffer();
			HQL.append("SELECT estacionamento FROM EstacionamentoPO estacionamento");
			HQL.append(" ");
			HQL.append("WHERE 1=1");

			montarHQLPrincipal(HQL, po);

			/*
			 * Criando e inicializando uma variavel respons�vel por criar uma
			 * Query com base na nossa HQL criada acima deixando-a preparada
			 * para o Hibernate executa-la.
			 */
			Query query = sessaoCorrente.createQuery(HQL.toString());

			/* Preenchendo os coringas da HQL */
			preecherCoringasHQLPrincipal(query, po);

			/*
			 * Executando a consulta list() = Consulta mais que um
			 * uniqueResult() = Consulta apenas 1 registro. Caso a HQL resulte
			 * em mais de um, uma excess�o ser� lan�ada.
			 */
			return (ArrayList<AbstractPO>) query.list();
		} catch (Exception e) {
			throw new FiltrarException("Erro ao filtrar", e);
		}

	}

	private void montarHQLPrincipal(StringBuffer HQL,
			EstacionamentoPO estacionamento) {

		if (!Utilidades.isNuloOuVazio(estacionamento.getCodigo())) {
			HQL.append(" ");
			HQL.append(" AND estacionamento.codigo = :codigoParam");
		}

		if (!Utilidades.isNuloOuVazio(estacionamento.getQuantidadeDeVagas())) {
			HQL.append(" ");
			HQL.append(" AND estacionamento.quantidadeDeVagas = :quantidadeDeVagasParam");
		}

		/** Verificando se a mercado foi enviado */
		if (!Utilidades.isNuloOuVazio(estacionamento.getMercado())) {
			montarHQLMercado(HQL, estacionamento.getMercado());
		}

		HQL.append(" ");
		HQL.append("order by estacionamento.nome");
	}

	private void montarHQLMercado(StringBuffer HQL, MercadoPO mercado) {
		if (!Utilidades.isNuloOuVazio(mercado.getNome())) {
			HQL.append(" ");
			HQL.append(" AND estacionamento.mercado.nome = :nomeParam");
		}

		if (!Utilidades.isNuloOuVazio(mercado.getEndereco())) {
			HQL.append(" ");
			HQL.append(" AND estacionamento.mercado.endereco = :enderecoParam");
		}

	}

	private void preecherCoringasHQLPrincipal(Query query,
			EstacionamentoPO estacionamento) {

		if (!Utilidades.isNuloOuVazio(estacionamento.getCodigo())) {
			query.setLong("codigoParam", estacionamento.getCodigo());
		}

		if (!Utilidades.isNuloOuVazio(estacionamento.getQuantidadeDeVagas())) {
			query.setByte("quantidadeDeVagasParam",
					estacionamento.getQuantidadeDeVagas());
		}

		/** Verificando se a mercado foi enviado */
		if (!Utilidades.isNuloOuVazio(estacionamento.getMercado())) {
			preecherCoringasHQLMercado(query, estacionamento.getMercado());
		}
	}

	private void preecherCoringasHQLMercado(Query query, MercadoPO mercado) {
		if (!Utilidades.isNuloOuVazio(mercado.getNome())) {
			query.setString("nomeParam", mercado.getNome());
		}

		if (!Utilidades.isNuloOuVazio(mercado.getEndereco())) {
			query.setString("enderecoParam", mercado.getEndereco());
		}

	}
}

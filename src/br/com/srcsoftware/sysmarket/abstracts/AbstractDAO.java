package br.com.srcsoftware.sysmarket.abstracts;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import br.com.srcsoftware.sysmarket.exceptions.AlterarException;
import br.com.srcsoftware.sysmarket.exceptions.CommitException;
import br.com.srcsoftware.sysmarket.exceptions.ExcluirException;
import br.com.srcsoftware.sysmarket.exceptions.InserirException;
import br.com.srcsoftware.sysmarket.exceptions.RollbackException;
import br.com.srcsoftware.sysmarket.exceptions.SessaoException;
import br.com.srcsoftware.sysmarket.exceptions.TransacaoException;
import br.com.srcsoftware.sysmarket.interfaces.InterfaceDAO;

/**
 * Classe abstrata responsav�l por gerenciar as requisi��es de conex�o ao banco
 * de dados. Utilizando o Hibernate para adquirir uma sess�o aberta.
 * 
 * @author Lucas Pedro Bermejo <lucas_bermejo@hotmail.com>
 * @since 12/12/2014 10:47:11
 * @version 1.0
 */
public abstract class AbstractDAO implements InterfaceDAO {

	/* UTILIZA��O DO HIBERNATE 4 */

	private static final SessionFactory SESSION_FACTORY;
	private static ServiceRegistry serviceRegistry;
	protected Session sessaoCorrente;
	private Transaction transacao;

	/**
	 * Bloco est�tico respons�vel por carregar um objeto "Configuration" com os
	 * dados configurados na TAG (<hibernate-configuration>) do arquivo
	 * hibernate.cfg.xml PS: Bloco est�tico garante que seu conte�do ser�
	 * executado pelo meno uma(1) vez ao instanciar a classe onde foi escrito.
	 */
	static {
		Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
		serviceRegistry = new ServiceRegistryBuilder().applySettings(
				cfg.getProperties()).buildServiceRegistry();
		SESSION_FACTORY = cfg.buildSessionFactory(serviceRegistry);
	}

	/**
	 * Polimorfico
	 * 
	 * @see br.com.srctreinamentotecnologico.interfaces.InterfaceDAO#abrirSessao()
	 */
	@Override
	public void abrirSessao() throws SessaoException {
		try {
			if (sessaoCorrente == null || !sessaoCorrente.isOpen()) {
				sessaoCorrente = SESSION_FACTORY.openSession();
			}
		} catch (Exception e) {
			throw new SessaoException("Problema ao abrir uma sess�o!", e);
		}

	}

	/**
	 * Informa que ua transa��o(persistencia) ser� feita no banco de dados.
	 * todas as transa��es devem ser confirmadas(commit) ou desfeitas(rollback)
	 * 
	 * Apenas a��es de persistencia necessitam de transa��o. Para consultas
	 * usamos apenas a sess�o.
	 * 
	 * Polimorfico
	 * 
	 * @see br.com.srctreinamentotecnologico.interfaces.InterfaceDAO#iniciarTransacao()
	 */
	@Override
	public void iniciarTransacao() throws TransacaoException, SessaoException {
		try {
			abrirSessao();

			transacao = sessaoCorrente.beginTransaction();
		} catch (SessaoException e) {
			throw e;
		} catch (Exception e) {
			throw new TransacaoException("Problema ao iniciar uma transa��o!",
					e);
		}
	}

	/**
	 * Polimorfico
	 * 
	 * @see br.com.srctreinamentotecnologico.interfaces.InterfaceDAO#fecharSessao()
	 */
	@Override
	public void fecharSessao() throws SessaoException {
		try {
			if (sessaoCorrente != null || sessaoCorrente.isOpen()) {
				sessaoCorrente.close();
				sessaoCorrente = null;
			}
		} catch (Exception e) {
			throw new SessaoException("Problema ao fechar uma sess�o!", e);
		}
	}

	/**
	 * Polimorfico
	 * 
	 * @see br.com.srctreinamentotecnologico.interfaces.InterfaceDAO#commitTransacao()
	 */
	@Override
	public void commitTransacao() throws CommitException {
		try {
			if (transacao != null) {
				transacao.commit();
			}
		} catch (Exception e) {
			throw new CommitException("Problema ao realizar o Commit!", e);
		}
	}

	/**
	 * Polimorfico
	 * 
	 * @see br.com.srctreinamentotecnologico.interfaces.InterfaceDAO#rollbackTransacao()
	 */
	@Override
	public void rollbackTransacao() throws RollbackException {
		try {
			if (transacao != null) {
				transacao.rollback();
			}
		} catch (Exception e) {
			throw new RollbackException("Problema ao realizar o Rollback!", e);
		}

	}

	/**
	 * Polimorfico
	 * 
	 * @see br.com.srctreinamentotecnologico.interfaces.InterfaceDAO#inserir(java.lang.Object)
	 */
	@Override
	public Long inserir(AbstractPO po) throws InserirException {
		try {
			/*
			 * A��o desejada a ser executada no banco de dados. SAVE - Ap�s
			 * inserir, o m�todo 'save' retorna o id no qual o objeto foi
			 * inserido.
			 */
			return (Long) sessaoCorrente.save(po);
		} catch (Exception e) {
			throw new InserirException("Erro ao inserir[" + po + "]", e);
		}

	}

	/**
	 * Polimorfico
	 * 
	 * @see br.com.srctreinamentotecnologico.interfaces.InterfaceDAO#alterar(java.lang.Object)
	 */
	@Override
	public void alterar(AbstractPO po) throws AlterarException {
		try {
			sessaoCorrente.update(po);
		} catch (Exception e) {
			throw new AlterarException("Erro ao alterar[" + po + "]", e);
		}

	}

	/**
	 * Polimorfico
	 * 
	 * @see br.com.srctreinamentotecnologico.interfaces.InterfaceDAO#excluir(java.lang.Object)
	 */
	@Override
	public void excluir(AbstractPO po) throws ExcluirException {
		try {
			sessaoCorrente.delete(po);
		} catch (Exception e) {
			throw new ExcluirException("Erro ao excluir[" + po + "]", e);
		}
	}

}

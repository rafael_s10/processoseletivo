<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		
			<!-- setando na sessao o caminho do WebContent -->	
			<% request.getSession().setAttribute( "root", request.getContextPath() ); %>
			
			<!-- IMPORTA��O DO JQUERY -->
			<script type="text/javascript" src="${root}/jquery/jquery-1.11.1.min.js"></script>
			<!-- IMPORTA��O DAS MASCARAS E DO TEMPLATE -->
  			<script type="text/javascript" src="${root}/jquery/mask/jquery.maskedinput.min.js"></script>
  			<script type="text/javascript" src="${root}/jquery/css/blitzer/jquery-ui-1.9.2.custom.min.js"></script>
  
			<!-- IMPORTA��O DAS FOLHAS DE ESTILO  CSS -->
			<link rel="stylesheet" type="text/css" href="${root}/jquery/css/blitzer/jquery-ui-1.9.2.custom.min.css" />
			<link rel="stylesheet" type="text/css" href="${root}/css/style_template.css" />
			<link rel="stylesheet" type="text/css" href="${root}/css/style_campos.css" />
			<link rel="stylesheet" type="text/css" href="${root}/css/style_botao.css" />
			<link rel="stylesheet" type="text/css" href="${root}/css/style_menu.css" />
			
		<title>Contato</title>
		
		<script type="text/javascript">
		$(document).ready(function(){
			$('#form_estacionamento').attr("autocomplete", "off");
			
			
			/*definindo campos obrigatorios */
			$('.obrigatorio').attr("required", "required");
			
			$(".datepicker").datepicker({
		         dateFormat:'dd/mm/yy',
		         dayNames: ['Domingo','Segunda','Ter�a','Quarta','Quinta','Sexta','S�bado'],
		         dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		         dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S�b','Dom'],
		         monthNames: ['Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		         monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		         nextText: 'Pr�ximo',
		         prevText: 'Anterior' 
		          });
		    
		    $(".datepicker").mask("99/99/9999");
		});
		
		function executar( nomeMetodo ){
		    document.getElementById("form_estacionamento").method.value = nomeMetodo;
			}

			function executarSubmit(nomeMetodo) {
				document.getElementById("form_estacionamento").method.value = nomeMetodo;
				document.getElementById("form_estacionamento").submit();
			}

			function checkRegexp(o, regexp, n) {
				if (!(regexp.test(o))) {
					return false;
				} else {
					return true;
				}
			}
		</script>
	</head>
	<body>
	<div id="wrapper">
			
			<!-- BANNER -->
			<div id="headerwrap">
				<div id="header">
					<img alt="banner_src" src="${root}/imagens/banner.png" style="border-radius: 10px 10px 0px 0px;">
				</div>
			</div>
			
			<!-- MENU -->
			<div id="navigationwrap">
				<div id="navigation">
					<div id="cssmenu" class="font_menu">
						<ul>
							<li class="active">
								<a href="${root}/index.html">
									<span>Sair</span>
								</a>
							</li>
							<li class="has-sub">
								<a href="#"><span>Quem sou?</span></a>
								
								<ul>
									<li class="last">
										<a href="#" onclick="alert('Nome: rafael\nArquiteto Java\nCel:(14) 9.9652-7007');">Desenvolvedor</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div><!-- FIM DO NAVIGATIONWRAP -->
			
			<!-- TELA TOTAL -->
			<div id="contentwrap">
			
				<!-- TELA -->
			    <div id="content" style="width:515px;">
					
					<!-- Mensagens Informativas -->
					<div class="ui-widget" style="text-align: center;">
						<logic:messagesPresent message="true">						
							<html:messages id="message" message="true">											
								<div>
									<p>
										<b><bean:write filter="false" name="message"/></b>	
									</p>
								</div>						
							</html:messages>
						</logic:messagesPresent>
					</div>
					
					<!-- TELA EM SI -->
					<div id="principal">
						<!-- Cabe�alho da Janela -->
						<header style="background-color: #3852A3; border-radius:5px 5px 0px 0px;">
							<div style="padding: 10px; font-family: Verdana; color:white; font-size: 18px;">
								<div style="float:left;">
									estacionamento
								</div>
								<div style="margin-left: 97%;">
									<a href="${root}/index.html">
										<img alt="Fechar" src="${root}/imagens/close_16_16.png">
									</a>
								</div>
							</div>
						</header>
						
						<!-- Corpo da Janela -->
						<section style="padding: 10px;">
							
							<!-- FAZER O FORMULARIO AQUI -->
							<html:form styleId="form_estacionamento" action="/estacionamentoAction.do" method="post">
								<html:hidden property="method" value="empty"/>
								
								<!--Linha - 1 -->
									
								<div id="Linha1">
									<!-- determina o conjunto label+ campo -->
									<div>
										<div>
											<label for="codigo">C�digo</label>
											<html:text styleId="codigo" styleClass="campo_formulario_centralizado" property="codigo" name="estacionamentoForm" style="width:414px" readonly="true"/>
										</div>
										
										<!-- determina o conjunto label+ campo -->
									<div>
										<div>
										<label for="Mercado">Mercado</label>           
            							<html:select styleClass="campo_formulario_centralizado" styleId="mercado" property="codigoMercado" name="mercadoForm" >
             							<html:optionsCollection name="mercadoForm" property="comboMercado" label="label" value="value" />
            							</html:select>
           					
										</div>										
									</div>
										
										
										
									</div>
									
									<div>
										<div>
											<label for="quantidadeDeVaga">Quantidade de vagas</label>
											<html:text  styleId="quantidadeDeVaga"  styleClass="campo_formulario_centralizado obrigatorio nome" property="quantidadeDeVaga" maxlength="10" name="estacionamentoForm" style="width:430px" />
										</div>
										
									</div>								
								</div>
								
								<!--Linha - 2 -->
									
								
								<div style="clear: both;"></div>

															<!-- Linha -->
							        <div id="linhaBotoes">
							         <!-- Determina o conjunto label + campo -->
							         <div>
							          <div>
							           <html:submit styleId="inserir" property="estacionamentoForm" title="Inserir" value="Inserir" styleClass="botao" onclick="executar('inserir');"/>
							           <html:submit styleId="alterar" property="estacionamentoForm" title="Alterar" value="Alterar" styleClass="botao" onclick="executar('alterar');"/>
							           <html:button property="estacionamentoForm" title="Excluir" value="Excluir" styleClass="botao" onclick="executarSubmit('excluir');"/>
							           <html:button property="estacionamentoForm" title="Filtrar" value="Filtrar" styleClass="botao" onclick="executarSubmit('filtrar');"/>
							           <html:button property="estacionamentoForm" title="Novo" value="Novo" styleClass="botao" onclick="executarSubmit('novo');"/>
							          </div>
							         </div>        
							        </div>
								
							</html:form>
							
							
						</section>
					</div>
					
			    </div>
			</div>
			<div id="footerwrap">
				<div id="footer">
					<p>Desenvolvido por SRC Treinamento Tecnol�gico LTDA ME - Java Aplicado VIII</p>
		            <p>SysAgenda &copy; - Todos os direitos reservados.</p>
				</div>
			</div>
		</div>
	
	
	</body>
</html>